package com.company;

public class Main {

    public static void main(String[] args) {


        Student student1 = new Student("name1", "address1", null, null);
        Student student2 = new Student("name2", "address2", null, null);
        Student student3 = new Student("name3", "address3", null, null);
        Student student4 = new Student("name4", "address4", null, null);
        Student student5 = new Student("name5", "address5", null, null);


        student1.setNext(student2);
        student1.setHead(student1);
        student2.setNext(student3);
        student2.setHead(student1);
        student3.setNext(student4);
        student3.setHead(student1);
        student4.setNext(student5);
        student4.setHead(student1);
        student5.setHead(student1);


        printInfos(student3.getHead());
    }

    private static void printInfos(Student student) {

        if (student != null) {
            System.out.println("name:" + student.getName() + ",address: " + student.getAddress());
            printInfos(student.getNext());
        }
    }
}
