package com.company;

public class Student {

    private String name;
    private String address;

    private Student head;
    private Student next;

    public Student(String name, String address, Student head, Student next) {
        this.name = name;
        this.address = address;
        this.head = head;
        this.next = next;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Student getHead() {
        return head;
    }

    public void setHead(Student head) {
        this.head = head;
    }

    public Student getNext() {
        return next;
    }

    public void setNext(Student next) {
        this.next = next;
    }
}
